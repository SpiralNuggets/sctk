import sys
import currency_exchange
import tangogame
import temp_convert
import about
from PySide6.QtWidgets import *
from PySide6.QtCore import *

class MotherWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self, None)
        self.setWindowTitle("Conversion Toolkit")
        self.setGeometry(100, 100, 500, 500)
        self.setCentralWidget(MotherTabWidget(self))

        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addAction("&Exit", self.close)
        helpMenu = self.menuBar().addMenu("&Help")
        helpMenu.addAction("&About", lambda: about.AboutPage().exec())

        self.show()

class MotherTabWidget(QTabWidget):
    def __init__(self, parent):
        QTabWidget.__init__(self, parent)
        self.setWindowTitle("Conversion Toolkit")
        self.setGeometry(100, 100, 500, 500)
        self.addTab(currency_exchange.currencyExchange(), "Currency Exchange")
        self.addTab(tangogame.TangoGame(), "Tango Game")
        self.addTab(temp_convert.TempConverter(), "Temperature Converter")
        #TODO: figure out a way to pause tango game when switching tabs
        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MotherWindow()
    sys.exit(app.exec())