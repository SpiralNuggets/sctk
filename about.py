from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *

import sys

class AboutPage(QMessageBox):
    def __init__(self):
        QMessageBox.__init__(self, None)
        self.setWindowTitle("About")
        self.setText("""Spiral's Conversion Toolkit
        
        This is a simple conversion toolkit that I made for my own use.
        
        Check out the LICENSE file for more information, if there's any.""")

        self.okButton = QPushButton(self)
        self.okButton.setText("OK")
        self.okButton.clicked.connect(lambda: self.close())

        self.notOkButton = QPushButton(self)
        self.notOkButton.setText("Not OK")
        self.notOkButton.clicked.connect(lambda: self.OnoderaLmao().exec())

        self.addButton(self.okButton, QMessageBox.AcceptRole)
        self.addButton(self.notOkButton, QMessageBox.RejectRole)

        self.show()
    
    # OnoderaLmao class omitted due to being not fit in a homework document.

    class OnoderaLmao(QWidget):
        def __init__(self):
            QWidget.__init__(self, None)
            self.setWindowTitle("Onodera Lmao")
            self.setGeometry(100, 100, 500, 500)
            self.onoderaLmaoLabel = QLabel()
            self.onoderaLmaoLabel.setPixmap(QPixmap("resource/onoderalmao.jpg"))
            anylayoutwilldo = QVBoxLayout()
            anylayoutwilldo.addWidget(self.onoderaLmaoLabel)
            self.setLayout(anylayoutwilldo)
            self.show()
        
        def closeEvent(self, event):
            self.close()

        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = AboutPage()
    sys.exit(app.exec())